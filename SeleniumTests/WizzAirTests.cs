﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTests
{
    [TestFixture]
    public class TestWizzair
    {
        [Test]
        public void WizzAirTests()
        {
            IWebDriver driver = new ChromeDriver(@"D:\Программы\chromedriver");
            driver.Manage().Window.Maximize();

            driver.Navigate().GoToUrl("https://wizzair.com");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            IWebElement delete_down_bar = driver.FindElement(By.XPath("//button[@class = 'cookie-policy__button']"));
            delete_down_bar = driver.FindElement(By.XPath("//button[@class = 'cookie-policy__button']"));
            delete_down_bar.Click();


            IWebElement departure_station = driver.FindElement(By.Id("search-departure-station"));
            departure_station.Clear();
            string dep_station = "Kiev";
            departure_station.SendKeys(dep_station);

            IWebElement departure_location_name = driver.FindElement(By.CssSelector("strong.locations-container__location__name"));
            departure_location_name = driver.FindElement(By.CssSelector("strong.locations-container__location__name"));
            departure_location_name.Click();

            IWebElement arrival_station = driver.FindElement(By.Id("search-arrival-station"));
            arrival_station.Clear();
            string arr_station = "Copenhagen";
            arrival_station.SendKeys(arr_station);

            IWebElement arrival_location_name = driver.FindElement(By.CssSelector("strong.locations-container__location__name"));
            arrival_location_name = driver.FindElement(By.CssSelector("strong.locations-container__location__name"));
            arrival_location_name.Click();

            IWebElement current_date = driver.FindElement(By.Id("search-departure-date"));
            current_date.Click();

            IWebElement min_date =driver.FindElement(By.XPath("//div[@class = 'rf-input__input']"));
            DateTime verify_min_data = DateTime.Parse(min_date.Text);

            IWebElement passengers = driver.FindElement(By.Id("search-passenger"));
            passengers.Click();

            IWebElement adult = driver.FindElement(By.XPath("//div[@class = 'stepper__content']"));
            adult = driver.FindElement(By.XPath("//div[@class = 'stepper__content']/*[text()='adult']/../input"));
            int count_of_adults;
            Int32.TryParse(adult.GetAttribute("value"), out count_of_adults);
            Assert.AreEqual(1, count_of_adults, "Adult = 1");

            IWebElement add_adult_child_infant = driver.FindElement(By.XPath("//div[@class = 'stepper stepper--flight-search']/button[2]"));

            IWebElement child = driver.FindElement(By.XPath("//div[@class = 'stepper__content']"));
            child = driver.FindElement(By.XPath("//div[@class = 'stepper__content']/*[text()='child']/../input"));
            int count_of_child;
            Int32.TryParse(child.GetAttribute("value"), out count_of_child);
            Assert.AreEqual(0, count_of_child, "Child = 0");

            IWebElement infant = driver.FindElement(By.XPath("//div[@class = 'stepper__content']"));
            infant = driver.FindElement(By.XPath("//div[@class = 'stepper__content']/*[text()='infant']/../input"));
            int count_of_infant;
            Int32.TryParse(infant.GetAttribute("value"), out count_of_infant);
            Assert.AreEqual(0, count_of_infant, "Child = 0");

            IWebElement click_ok = driver.FindElement(By.XPath("//div[@class = 'flight-search__panel__done-btn gutter-bottom']/button[@class = 'rf-button rf-button--medium rf-button--primary']"));
            click_ok.Click();

            IWebElement click_search = driver.FindElement(By.XPath("//div[@class = 'flight-search__panel__fieldset flight-search__panel__fieldset--cta-btn']"));
            click_search.Click();

            int count_tabs = driver.WindowHandles.Count();
            if (count_tabs > 1)
            {
                driver.SwitchTo().Window(driver.WindowHandles[0]).Close();
                driver.SwitchTo().Window(driver.WindowHandles[0]);
            }

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(2);
            IWebElement temp_date = driver.FindElement(By.XPath("//label[@class = 'booking-flow__flight-select__chart__day__label booking-flow__flight-select__chart__day__label--selected']//time"));
            DateTime verify_temp_date = DateTime.Parse(temp_date.Text);
            Assert.AreEqual(verify_min_data, verify_temp_date, "Check data");

            IWebElement display_date = driver.FindElement(By.XPath("//time[@class = 'heading heading--4 date']"));
            DateTime verify_display_date = DateTime.Parse(display_date.Text);
            Assert.AreEqual(verify_min_data, verify_display_date, "Check data");

            IReadOnlyList<IWebElement> prices = driver.FindElements(By.XPath("//label[@class = 'rf-fare__main rf-fare__main--with-slogan']"));
            Assert.AreEqual(3, prices.Count(), "Check prices");

            IWebElement display_location = driver.FindElement(By.XPath("//address[@class = 'booking-flow__flight-select__title heading heading--3']"));
            Assert.That(display_location.Text, Contains.Substring(dep_station), "Check locatio");
            Assert.That(display_location.Text, Contains.Substring(arr_station));

            IWebElement return_button = driver.FindElement(By.XPath("//div[@class = 'booking-flow__flight-select__add-return-flight-container']"));
            Assert.IsTrue(return_button.Displayed);

            IReadOnlyList<IWebElement> chose_flight = driver.FindElements(By.CssSelector("td[class*=basic] label"));
            IWebElement basic_price = chose_flight.First();
            basic_price.Click();
      
            IWebElement click_continue1 = driver.FindElement(By.Id("flight-select-continue-btn"));
            click_continue1.Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            IWebElement first_name = driver.FindElement(By.Id("passenger-first-name-0"));
            first_name.SendKeys("Armen");

            IWebElement second_name = driver.FindElement(By.Id("passenger-last-name-0"));
            second_name.SendKeys("Armen");

            IWebElement gender = driver.FindElement(By.XPath("//div[@class = 'rf-switch rf-switch--small rf-switch--input-sized']/label[2]"));
            gender.Click();

            IWebElement location = driver.FindElement(By.XPath("//span[@class = 'tab__title title title--2']"));
            StringAssert.Contains(dep_station.ToLower(), location.Text.ToLower());
            StringAssert.Contains(arr_station.ToLower(), location.Text.ToLower());

            IWebElement none_luggage_option = driver.FindElement(By.XPath("//*[@class = 'rf-switch__label baggage-switcher--0']/*[@class = 'rf-switch__label__inner']"));
            none_luggage_option.Click();

            IWebElement click_continue2 = driver.FindElement(By.Id("passengers-continue-btn"));
            click_continue2.Click();

            IWebElement sign_in = driver.FindElement(By.Id("login-modal"));
            Assert.IsTrue(sign_in.Displayed);


            driver.Quit();
        }

    }
}
